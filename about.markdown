---
layout: page
title: About Phluxus
permalink: /about/
---

# Phluxus Blog

Phluxus develops software for macOS and iOS (and all the other Apple platforms), and has affinities for hardware hacking.

Here I try to document some hardware stuff.

[Phluxus main website](https://www.phlux.us)